<?php

/**
 * Critere {intervalle_inclu}
 *
 * Sélectionne des éléments à l'intérieur de bornes gauches et droites
 * donnees dans une boucle supérieure ou dans l'environnement.
 * 
 * Retourne les enfants entre les 2 bornes ainsi que le père éventuel.
**/
function critere_intervalle_inclu_dist($idb, &$boucles, $crit){
	return critere_intervalle_dist($idb, $boucles, $crit, true);
}

/**
 * Critere {intervalle}
 *
 * Sélectionne des éléments à l'intérieur de bornes gauches et droites
 * donnees dans une boucle supérieure ou dans l'environnement.
 * 
 * Ne retourne que les enfants entre les 2 bornes.
**/
function critere_intervalle_dist($idb, &$boucles, $crit, $inclu=false){
	$_gauche = calculer_argument_precedent($idb, 'gauche', $boucles);
	$_droit = calculer_argument_precedent($idb, 'droit', $boucles);
	$gauche = kwote($_gauche);
	$droit = kwote($_droit);
	$boucle = &$boucles[$idb];

	// noeud principal inclu ? {intervalle_inclu}
	if ($inclu) {
		$op_gauche = "'>='";
		$op_droit = "'<='";
	} else {
		$op_gauche = "'>'";
		$op_droit = "'<'";
	}

	// la question fondammentale est :
	// que faire si {intervalle} n'a pas de gauche/droit defini
	// dans l'environnement ou dans un parent ?
	// tout afficher ou ne rien afficher ?
	// ...
	// bien on dit que {intervalle} = on affiche rien
	// et {intervalle ?} = on affiche tout... simple...

	$where = array("'AND'",
		array($op_gauche, "'$boucle->id_table.gauche'", $gauche),
		array($op_droit, "'$boucle->id_table.droit'", $droit),
	);

	// si {intervalle ?} alors on affiche tout si gauche ou droit n'est pas connu
	if ($crit->cond) {
		$where = array("'?'", $_gauche, array("'?'", $_droit, $where, "''"), "''");
	}
	$boucle->where[] = $where;
}

?>
