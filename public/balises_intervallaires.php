<?php


/**
 * Retourne le nombre d'elements enfants d'un noued intervallaire donné
 * en s'appuyant sur la différence entre le bord droit et le bord gauche
 * $n = (bd - bg  - 1) / 2 
 *
 * @param 
 * @return 
**/
function balise_NOMBRE_ELEMENTS_dist($p) {
	$_gauche = champ_sql('gauche', $p);
	$_droit = champ_sql('droit', $p);
	$p->code = "(($_droit - $_gauche - 1) / 2)";
	return $p;
}


?>
