<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// I
	'intervallaire' => "Intervallaire",

	// E
	'erreur_parent_insolvable' => "Le parent choisi n'est pas cohérent.
		Je ne peux pas être mon propre père ni le fils de mon enfant !",

);

?>
