<?php
/**
 * SQL Intervallaire pour SPIP
 * Licence GPL (c) 2010 Matthieu Marcillaud 
**/


/**
 * Classe pour faciliter la gestion d'arbres intervallaires en SQL
 * avec l'abstraction sql de SPIP
 *
 * Fonctions publiques :
 * 
 * - inserer_element
 * - supprimer_element
 * - deplacer_element
 * - deplacer_arbre
 * 
 * - declarer_champs_sql
 * 
 * - regenerer_bornes
 * - regenerer_parents
 * 
 * - est_deplacable_sur_parent
 * 
**/
class sql_intervallaire {


	/**
	 * Ajoute les champs SQL sur la déclaration d'une table
	 * afin d'en permettre la gestion d'arborescence intervallaire.
	 *
	 * @param array $tables : tableau de déclaration des tables principales ou auxiliaires
	 * @param string $table : nom de la table sur laquelle ajouter les champs sql pour la gestion intervallaire
	 * @return array : déclaration des tables modifiée.
	**/
	static public function declarer_champs_sql($tables, $table) {
		
		if (!isset($tables[$table])) {
			spip_log('SQL Intervallaire doit declarer des champs SQL sur "' . $table
				. '" mais cette table n\'est pas déclarée ! Opération annulée :)');
			return $tables;
		}
		
		$table = &$tables[$table];

		$champs = array(
			"id_parent" => "bigint(21) NOT NULL",
			
			"gauche" => "bigint(21) NOT NULL",
			"droit"  => "bigint(21) NOT NULL",
			
			"rang" => "bigint(21) NOT NULL",
			"profondeur" => "bigint(21) NOT NULL"
		);
		
		$table['field'] = array_merge($table['field'], $champs);

		// mettre des cles sur gauche et droit ?
		return $tables;
	}






	/**
	 * Regénère pour une table donnée l'ensemble des bornes gauches et droites
	 * des éléments présents en s'appuyant sur les valeurs des 'id_parent'
	 * et des rangs connus.
	 * Régénère aussi le champ 'profondeur'
	 *
	 * @param string $table Table SQL dont on souhaite régénérer les bornes
	 * @return int Nombre d'éléments de la table
	**/
	static public function regenerer_bornes($table) {
		$table = table_objet_sql($table);
		$id_table = id_table_objet($table);
		$droit = sql_intervallaire::_regenerer_bornes($table, $id_table);
		return (int)($droit / 2); // nombre d'elements.
	}


	/**
	 * Fonction privée récursive pour régénérer les bornes sur une partie d'une table SQL
	 *
	 * @param string $table 	Nom de la table SQL
	 * @param string $id_table 	Nom de la clé primaire
	 * @param int $id_parent 	Identifinant du parent à traiter
	 * @param int $gauche 		Valeur actuelle du bord gauche du parent
	 * @param int $droit 		Valeur actuelle du bord droit du parent
	 * @param int $profondeur 	Profondeur actuelle.
	 * 
	 * @return int Valeur du dernier bord droit insere.
	**/
	static private function _regenerer_bornes($table, $id_table, $id_parent=0, $gauche=0, $droit=1, $profondeur=1) {
		$results = sql_select($id_table, $table, array('id_parent=' . sql_quote($id_parent)), '', 'rang ASC');
		while ($res = sql_fetch($results)) {
			// on a des enfants... la gauche augmente de 1,
			// la droite prend la valeur de gauche +1
			$gauche++;
			#$droit = $gauche + 1;
			
			// reccursion
			$droit = sql_intervallaire::_regenerer_bornes($table, $id_table, $res[$id_table], $gauche, $gauche+1, $profondeur+1);

			// si la droite a  ete modifiee, c'est qu'il y a des petits enfants ;
			// a ce moment la, il faut incrementer droite d'encore 1.
			if ($gauche + 1 != $droit) {
				$droit ++;
			}
			
			// traitements
			sql_updateq($table, array(
				'gauche'=>$gauche,
				'droit'=>$droit,
				'profondeur'=>$profondeur),
				"$id_table=". sql_quote($res[$id_table]));

			// definir gauche sur droit...
			$gauche = $droit;
		}
		return $droit;
	}



	/**
	 * Regénère pour une table donnée l'ensemble des id_parent, rang et profondeur
	 * des éléments présents en s'appuyant sur les valeurs des bornes gauches et droits présents.
	 *
	 * @param string $table Table SQL dont on souhaite régénérer les id_parent
	 * @return
	**/
	static public function regenerer_parents($table) {
		$table = table_objet_sql($table);
		$id_table = id_table_objet($table);

		$parent = 0;
		$profondeur = 1;
		$rangs = $droits = array();
		
		$results = sql_select(array($id_table, 'gauche', 'droit'), $table, '', 'gauche ASC');
		while ($res = sql_fetch($results)) {

			// si le gauche recupere correspond a un frere suivant un droit connu,
			// c'est a dire un frere ayant eu des enfants, on recharge sa profondeur
			// et son parent
			if (isset($droits[$res['gauche'] - 1])) {
				list($parent, $profondeur) = $droits[$res['gauche'] - 1];
			}
			$rangs[$parent]++; 

			// traitements
			sql_updateq($table, array(
				'id_parent' => $parent,
				'profondeur' => $profondeur,
				'rang' => $rangs[$parent] * 10),
				"$id_table=". sql_quote($res[$id_table]));
				
			// si la gauche et la droite font un écart... c'est que l'on va avoir des enfants...
			if ($res['droit'] - $res['gauche'] != 1) {
				// on stocke la valeur du bord droit, pour retrouver la profondeur ensuite...
				// si un eventuel frère est rencontre plus tard...
				$droits[$res['droit']] = array($parent, $profondeur);
				$profondeur++;
				$parent = $res[$id_table];
			}
		}
	}





	/**
	 * Teste si un élément peut être déplacé
	 * dans l'élément parent indiqué.
	 * Renverra false si le parent est un des enfants de l'élément indiqué.
	 *
	 * @param string $table		Table SQL
	 * @param int $id_objet		Identifiant de l'élément déplacé
	 * @param int $id_parent_futur	Identifiant du futur parent.
	 * @return bool/null : null est indecidable (id_objet non valide). true:possible, false:impossible (du moins difficile).
	**/
	static public function est_deplacable_sur_parent($table, $id_objet, $id_parent_futur) {
		$table = table_objet_sql($table);
		$id_table = id_table_objet($table);

		if (!$id_objet = intval($id_objet)) {
			return null; // insondable
		}
		if (!$id_parent_futur = intval($id_parent_futur)) {
			return true; // on peut forcement deplacer l'element a la racine (id_parent=0)
		}
		
		// verifier la pertinence du parent choisi
		$me = sql_fetsel('gauche, droit', $table, "$id_table=" . sql_quote($id_gens));
		$parent = sql_fetsel('gauche, droit', $table, "$id_table=" . sql_quote($id_parent_futur));
		if (($parent['gauche'] >= $me['gauche']) and ($parent['droit'] <= $me['droit'])) {
			// le parent defini est moi ou un de mes enfants...
			// cas difficile a traiter
			return false;
		}
		return true;
	}



	/**
	 * Régénère les rang des enfants d'un parent donné
	 * en comptant de 10 en 10
	 *
	 * @param $table	La table
	 * @param $id_parent	Identifiant parent...
	 * @return bool
	**/
	static private function _regenerer_rang($table, $id_parent) {
		$id_table = id_table_objet($table);
		
		// remettre les rangs dans un ordre coherent par rapport a gauche...
		$results = sql_select($id_table, $table, array('id_parent = ' . $id_parent), '', 'gauche ASC');
		$rang = 0;
		while ($res = sql_fetch($results)) {
			$rang += 10;
			sql_updateq($table, array('rang' => $rang), array("$id_table=".$res[$id_table]));
		}
	}
	


	/**
	 * Crée un élément dans un arbre intervallaire.
	 * Cet élément est positionné par rappord à un noeud (id_cible)
	 *
	 * @param string $table		Table SQL
	 * @param int $id_cible		Identifiant du referant de l'insersion (peut être un parent ou un frère)
	 * @param array  $couples		Tableau facultatif de 'cle'=>'valeur' pour transmettre du contenu a l'insertion array('nom'=>'Tommy', 'age'=>18)...
	 * @param string $ou		Où s'effectue l'insertion ? droite/gauche
	 * @param string $type		Type de la cible ? parent/frere
	 * 
	 * @return int Valeur de l'identifiant créé.
	 * 
	**/
	static public function inserer_element($table, $id_cible=0, $couples=array(), $ou='droite', $type='parent') {
		$table = table_objet_sql($table);
		$id_cible = intval($id_cible);

		// test des parametres transmis...
		if (!in_array($ou, array('droite', 'gauche'))) {
			spip_log("sql_intervallaire::inserer_element() Mauvais parametre \$ou:$ou (seuls 'droite' ou 'gauche' sont admis)");
			return false;			
		}
		
		if (!in_array($type, array('parent', 'frere'))) {
			spip_log("sql_intervallaire::inserer_element() Mauvais parametre \$type:$type (seuls 'parent' ou 'frere' sont admis)");
			return false;			
		}

		// classe de gestion de transactions sql
		include_spip('base/sql_transaction');
		
		// premier cas le plus simple : id_cible = 0
		// on n'a pas de parent a gerer...
		if (!$id_cible) {
			
			// dans ce cas la d'ailleurs, le type ne peut pas être 'frere'
			// puisque le noeud n'existe pas !
			if ($type != 'parent') {
				spip_log("sql_intervallaire::inserer_element() Ne peut insérer un élément frère si aucun noeud n'est donné");
				return false;
			}

			$type = 'racine';
		}

		// creer le nom de fonction a utiliser. Ex : '_inserer_element_a_droite_de_racine'
		$inserer_element = '_inserer_element_a_' . $ou . '_de_' . $type;
		return sql_intervallaire::$inserer_element($table, $id_cible, $couples);

	}



	/**
	 * Insère un élément à droite de la racine
	 *
	 * @param string $table		Table SQL
	 * @param unused $null		Paramètre transmis non utilisé (id_parent nul)
	 * @param array $couples	Couples de cle/valeurs a inserer
	 * @return int 				Nouvel identifiant inseré.
	**/
	static private function _inserer_element_a_droite_de_racine($table, $unused = null, $couples=array()) {
		$frere = sql_fetsel('rang, droit', $table, 'profondeur=1', '', 'droit DESC', '0,1');
		if (!$frere) {
			$frere = array('rang' => 0, 'droit' => 0);
		}
		return sql_insertq($table, array_merge($couples, array(
			'id_parent'  => 0,
			'profondeur' => 1,
			'gauche'     => $frere['droit'] + 1,
			'droit'      => $frere['droit'] + 2,
			'rang'       => $frere['rang'] + 10,
		)));
	}



	/**
	 * Insère un élément à gauche de la racine
	 *
	 * @param string $table		Table SQL
	 * @param unused $null		Paramètre transmis non utilisé (id_parent nul)
	 * @param array $couples	Couples de cle/valeurs a inserer
	 * @return int 				Nouvel identifiant inseré.
	**/
	static private function _inserer_element_a_gauche_de_racine($table, $unused = null, $couples=array()) {
		// on decale tout de 2 dans une transaction...
		$sql = new sql_transaction();
		$sql->update($table, array(
			'droit'  => 'droit + 2',
			'gauche' => 'gauche + 2'), "gauche >= 1");
		$sql->insertq($table,  array_merge($couples, array(
			'gauche'     => 1,
			'droit'      => 2,
			'profondeur' => 1,
			'rang'       => 0,
			'id_parent'  => 0,
		)));
		// redefinir les rangs.
		$sql->update($table, array('rang'=>'rang + 10'), "id_parent = 0");
		if ($sql->run()) {
			$id_table = id_table_objet($table);
			return sql_getfetsel($id_table, $table, array('gauche=1', 'droit=2'));
		}
		return false;
	}	





	/**
	 * Retourne pour un identifiant de noeud donné les informations
	 * id, rang, profondeur, gauche et droite. 
	 *
	 * @param string $table		Table SQL
	 * @param int $id_noeud		Identifiant du noeud
	 * @return array			Informations sur le noeud.
	**/
	static public function get_noeud_infos($table, $id_noeud) {
		$id_table = id_table_objet($table);

		$noeud = sql_fetsel("$id_table AS id, $id_table, gauche, droit, profondeur, rang, id_parent", $table, "$id_table=" . sql_quote($id_noeud));
		if (!$noeud) {
			spip_log("sql_intervallaire: Le noeud demandé ($id_noeud) est inconnu.");
			return false;
		}
		return $noeud;
	}



	/**
	 * Insère un élément à droite d'un parent donné
	 *
	 * @param string $table		Table SQL
	 * @param int $id_parent	Identifiant du parent
	 * @param array $couples	Couples de cle/valeurs a inserer
	 * @return int 				Nouvel identifiant inseré.
	**/
	static private function _inserer_element_a_droite_de_parent($table, $id_parent, $couples=array()) {
		// pas de parent ? pas de chocolat
		if (!$parent = sql_intervallaire::get_noeud_infos($table, $id_parent)) {
			return false;
		}
		
		$id_table   = id_table_objet($table);
		$gauche     = $parent['gauche'];
		$droit 	    = $parent['droit'];
		$profondeur = $parent['profondeur'];
		
		// si le parent a des enfants... on definit un rang superieur pour l'insertion
		if ($droit - $gauche > 1) {
			$rang = sql_getfetsel('rang', $table,
				array('profondeur=' . $profondeur + 1, 'id_parent=' . $id_parent), '', 'rang DESC', '0,1');
			$rang += 10;
		} else {
			$rang = 10;
		}

		// l'insertion consiste a décaler tout ce qui est à droite du bord droit de 2 points.
		$sql = new sql_transaction();
		$sql->update($table, array('droit'  => 'droit + 2'),  "droit >= $droit");
		$sql->update($table, array('gauche' => 'gauche + 2'), "gauche >= $droit");
		$sql->insertq($table, array_merge($couples, array(
			'gauche'     => $droit,
			'droit'      => $droit + 1,
			'profondeur' => $profondeur + 1,
			'rang'       => $rang,
			'id_parent'  => $id_parent,
		)));
		if ($sql->run()) {
			return  sql_getfetsel($id_table, $table, array('gauche='.$droit, 'droit='. ($droit + 1)));
		}
		return false;
	}




	/**
	 * Insère un élément à gauche d'un parent donné
	 *
	 * @param string $table		Table SQL
	 * @param int $id_parent	Identifiant du parent
	 * @param array $couples	Couples de cle/valeurs a inserer
	 * @return int 				Nouvel identifiant inseré.
	**/
	static private function _inserer_element_a_gauche_de_parent($table, $id_parent, $couples=array()) {
		// pas de parent ? pas de chocolat
		if (!$parent = sql_intervallaire::get_noeud_infos($table, $id_parent)) {
			return false;
		}
		
		$id_table   = id_table_objet($table);
		$gauche     = $parent['gauche'];
		$droit 	    = $parent['droit'];
		$profondeur = $parent['profondeur'];

		// l'insertion consiste a decaler tout ce qui est a droite du bord gauche de 2 points.
		$rang = 0;
		$sql = new sql_transaction();
		$sql->update($table, array('droit'  => 'droit + 2'),  "droit > $gauche");
		$sql->update($table, array('gauche' => 'gauche + 2'), "gauche > $gauche");
		$sql->insertq($table,  array_merge($couples, array(
			'gauche'     => $gauche + 1,
			'droit'      => $gauche + 2,
			'profondeur' => $profondeur + 1,
			'rang'       => $rang,
			'id_parent'  => $id_parent,
		)));
		$sql->update($table, array('rang' => 'rang + 10'), array('id_parent = ' . $id_parent));
		if ($sql->run()) {
			return  sql_getfetsel($id_table, $table, array('gauche='.$gauche + 1, 'droit='.$gauche + 2));
		}
		return false;		
	}

	
	/**
	 * Insère un élément à droite d'un frère donné
	 *
	 * @param string $table		Table SQL
	 * @param int $id_frere		Identifiant du frère
	 * @param array $couples	Couples de cle/valeurs a inserer
	 * @return int 				Nouvel identifiant inseré.
	**/	
	static private function _inserer_element_a_droite_de_frere($table, $id_frere, $couples=array()) {
		// pas de frere ? pas de chocolat
		if (!$frere = sql_intervallaire::get_noeud_infos($table, $id_frere)) {
			return false;
		}

		$id_table   = id_table_objet($table);
		$gauche     = $frere['gauche'];
		$droit 	    = $frere['droit'];
		$profondeur = $frere['profondeur'];
		$rang       = $frere['rang'];
		$id_parent  = $frere['id_parent'];
		
		// l'insertion consiste a décaler tout ce qui est strictement à droite du bord droit de 2 points.
		$sql = new sql_transaction();
		$sql->update($table, array('droit'  => 'droit + 2'),  "droit > $droit");
		$sql->update($table, array('gauche' => 'gauche + 2'), "gauche > $droit");
		$sql->update($table, array('rang' => 'rang + 10'), array("id_parent = $id_parent", "rang > ". $rang));
		$sql->insertq($table, array_merge($couples, array(
			'gauche'     => $droit + 1,
			'droit'      => $droit + 2,
			'profondeur' => $profondeur,
			'rang'       => $rang + 10,
			'id_parent'  => $id_parent,
		)));
		if ($sql->run()) {
			return  sql_getfetsel($id_table, $table, array('gauche='. ($droit + 1), 'droit='. ($droit + 2)));
		}
		return false;		

		
	}

	
	/**
	 * Insère un élément à gauche d'un frère donné
	 *
	 * @param string $table		Table SQL
	 * @param int $id_frere		Identifiant du frère
	 * @param array $couples	Couples de cle/valeurs a inserer
	 * @return int 				Nouvel identifiant inseré.
	**/	
	static private function _inserer_element_a_gauche_de_frere($table, $id_frere, $couples=array()) {
		// pas de frere ? pas de chocolat
		if (!$frere = sql_intervallaire::get_noeud_infos($table, $id_frere)) {
			return false;
		}

		$id_table   = id_table_objet($table);
		$gauche     = $frere['gauche'];
		$droit 	    = $frere['droit'];
		$profondeur = $frere['profondeur'];
		$rang       = $frere['rang'];
		$id_parent  = $frere['id_parent'];
		
		// l'insertion consiste a décaler tout ce qui est à droite du bord gauche de 2 points.
		$sql = new sql_transaction();
		$sql->update($table, array('droit'  => 'droit + 2'),  "droit >= $gauche");
		$sql->update($table, array('gauche' => 'gauche + 2'), "gauche >= $gauche");
		$sql->update($table, array('rang' => 'rang + 10'), array("id_parent = $id_parent", "rang >= " . $rang));
		$sql->insertq($table, array_merge($couples, array(
			'gauche'     => $gauche,
			'droit'      => $gauche + 1,
			'profondeur' => $profondeur,
			'rang'       => $rang,
			'id_parent'  => $id_parent,
		)));
		if ($sql->run()) {
			return  sql_getfetsel($id_table, $table, array('gauche='. $gauche, 'droit='. ($gauche + 1)));
		}
		return false;
	}





	/**
	 * Supprime un noeud de l'arbre.
	 * De façon récursive (avec ses enfants) ou non. 
	 *
	 * @param string $table 	Table SQL
	 * @param int $id_noued 	Identifiant de l'element a supprimer
	 * @param bool $recursif 	Recursivement ?
	 * @return bool.
	**/
	static public function supprimer_element($table, $id_noeud, $recursif = false) {

		// classe de gestion de transactions sql
		include_spip('base/sql_transaction');
		
		if ($recursif) {
			return sql_intervallaire::_supprimer_element_recursif($table, $id_noeud);
		} else {
			return sql_intervallaire::_supprimer_element_unique($table, $id_noeud);
		}
	}


	/**
	 * Supprime un noeud de l'arbre de façon récursive. 
	 *
	 * @param string $table 	Table SQL
	 * @param int $id_noued 	Identifiant de l'element a supprimer
	 * @return bool.
	**/
	static private function _supprimer_element_recursif($table, $id_noeud) {
		if (!$noeud = sql_intervallaire::get_noeud_infos($table, $id_noeud)) {
			return false;
		}
		
		$id_table = id_table_objet($table);
		$gauche   = $noeud['gauche'];
		$droit    = $noeud['droit'];
		$delta    = $droit - $gauche + 1;
		
		// la suppression consiste a enlever tous les elements entre les bornes du noeud
		// et a decaler les bornes
		$sql = new sql_transaction();
		$sql->delete($table, array('gauche >= ' . $gauche, 'droit <= ' . $droit));
		$sql->update($table, array('gauche' => 'gauche - ' . $delta), "gauche >= $gauche");
		$sql->update($table, array('droit'  => 'droit - ' . $delta),  "droit >= $gauche");
		return $sql->run();	
	}


	/**
	 * Supprime un noeud de l'arbre et remonte les éventuels enfants. 
	 *
	 * @param string $table 	Table SQL
	 * @param int $id_noued 	Identifiant de l'element a supprimer
	 * @return bool.
	**/
	static private function _supprimer_element_unique($table, $id_noeud) {
		if (!$noeud = sql_intervallaire::get_noeud_infos($table, $id_noeud)) {
			return false;
		}

		$id_table  = id_table_objet($table);
		$gauche    = $noeud['gauche'];
		$droit     = $noeud['droit'];
		$id_parent = $noeud['id_parent'];
		$delta     = $droit - $gauche + 1;
				
		// si c'est une feuille... pas grand chose a realiser...
		if ($droit - $gauche == 1) {
			// c'est comme une suppression reccursive
			// la suppression consiste a enlever l'elements et a decaler les bornes
			$sql = new sql_transaction();
			$sql->delete($table, array('gauche >= ' . $gauche, 'droit <= ' . $droit));
			$sql->update($table, array('gauche' => 'gauche - ' . $delta), "gauche >= $gauche");
			$sql->update($table, array('droit'  => 'droit - ' . $delta),  "droit >= $gauche");
			return $sql->run();					
		}
		
		// si c'est un noeud... il faut remonter tous ses enfants d'un niveau...
		$sql = new sql_transaction();
		$sql->delete($table, array('gauche = ' . $gauche, 'droit = ' . $droit));
		// remonter le parent des premiers enfants
		$sql->update($table, array('id_parent' => $id_parent), array('id_parent =' . $noeud['id']));
		// redefinir toutes les profondeurs et bords gauche et droit de la famille
		$sql->update($table,
			array(
				'profondeur' => 'profondeur - 1',
				'gauche' => 'gauche - 1',
				'droit'  => 'droit - 1'),
			array(
				'gauche > ' . $gauche,
				'droit < ' . $droit)
			);
		// redéfinir les bornes supérieures à la famille affectée.
		$sql->update($table, array('gauche' => 'gauche - 2'), "gauche >= $droit");
		$sql->update($table, array('droit'  => 'droit - 2'), "droit >= $droit");
		if ($sql->run()) {
			sql_intervallaire::_regenerer_rang($table, $id_parent);
			return true;
		}
		return false;		
	}





	/**
	 * Déplace un élément unique (ou un arbre (noeud et enfants))
	 * 
	 * @param string $table 	Table SQL
	 * @param int $id_source 	Identifiant de l'element a déplacer
	 * @param int $id_cible 	Identifiant du futur parent ou frère
	 * @param string $ou	 	Où déplacer ? à gauche ou à droite
	 * @param string $type	 	Déplacement vers un parent ou un frère ?
	 * @param bool $recursif 	Déplacement récursif (tout l'arbre) ? ou simplement l'élément.
	 * @return bool.
	**/
	static public function deplacer_element($table, $id_source, $id_cible, $ou = 'droite', $type = 'parent', $recursif = false) {
		$table = table_objet_sql($table);
		$id_cible = intval($id_cible);
	
		// test des parametres transmis...
		if (!in_array($ou, array('droite', 'gauche'))) {
			spip_log("sql_intervallaire::deplacer_element() Mauvais parametre \$ou:$ou (seuls 'droite' ou 'gauche' sont admis)");
			return false;			
		}
		
		if (!in_array($type, array('parent', 'frere'))) {
			spip_log("sql_intervallaire::deplacer_element() Mauvais parametre \$type:$type (seuls 'parent' ou 'frere' sont admis)");
			return false;			
		}
		
		if ($id_source == $id_cible) {
			spip_log("sql_intervallaire::deplacer_element() Cible et source ne peuvent pas être identiques !");
			return false;			
		}

		// classe de gestion de transactions sql
		include_spip('base/sql_transaction');
		
		// premier cas le plus simple : id_cible = 0
		// on n'a pas de parent a gerer...
		if (!$id_cible) {
			
			// dans ce cas la d'ailleurs, le type ne peut pas être 'frere'
			// puisque le noeud n'existe pas !
			if ($type != 'parent') {
				spip_log("sql_intervallaire::deplacer_element() Ne peut insérer un élément frère si aucun noeud n'est donné");
				return false;
			}

			$type = 'racine';
		}

		// creer le nom de fonction a utiliser. Ex : '_deplacer_element_a_droite_de_racine'
		$quoi = ($recursif) ?  'arbre' : 'element' ;
		$deplacer_element = '_deplacer_' . $quoi . '_a_' . $ou . '_de_' . $type;
		return sql_intervallaire::$deplacer_element($table, $id_source, $id_cible);
	}


	/**
	 * Déplace un arbre (noeud et enfants)
	 *
	 * @param string $table 	Table SQL
	 * @param int $id_source 	Identifiant de l'element a déplacer
	 * @param int $id_cible 	Identifiant du futur parent ou frère
	 * @param string $ou	 	Où déplacer ? à gauche ou à droite
	 * @param string $type	 	Déplacement vers un parent ou un frère ?
	 * @return bool.
	**/
	static public function deplacer_arbre($table, $id_source, $id_cible, $ou = 'droite', $type = 'parent') {
		return sql_intervallaire::deplacer_element($table, $id_source, $id_cible, $ou,  $type, true);
	}



	/**
	 * Déplace un arbre (noeud et enfants) à droite de la racine. 
	 *
	 * @param string $table 	Table SQL
	 * @param int $id_source 	Identifiant de l'element a déplacer
	 * @param int $unused 		Identifiant cible (inutilisé)
	 * @return bool.
	**/
	static private function _deplacer_arbre_a_droite_de_racine($table, $id_source, $unused = null) {
		if (!$source = sql_intervallaire::get_noeud_infos($table, $id_source)) {
			return false;
		}

		$frere = sql_fetsel('rang, gauche, droit', $table, 'profondeur=1', '', 'droit DESC', '0,1');
		if (!$frere) {
			// il est impossible d'avoir a déplacer un élément
			// alors qu'il n'y a aucun élément racine !
			return false;
		}

		$id_table = id_table_objet($table);
		$gauche   = $source['gauche'];
		$droit    = $source['droit'];
		$delta    = $droit - $gauche + 1;
		
		$deltap   = $source['profondeur'] - 1;
		$deltag   = $frere['gauche'] - $gauche + 2;
		
		// decaler la source sur le bord racine droit
		// diminuer du delta tout ce qui est a droite du bord gauche
		$sql = new sql_transaction();
		$sql->update($table,
			array(
				'id_parent'     => 0,
			),
			array(
				'gauche = ' . $gauche,
				'droit = ' . $droit,
		));
		$sql->update($table,
			array(
				'gauche'     => 'gauche + ' . $deltag,
				'droit'      => 'droit + ' . $deltag,
				'profondeur' => 'profondeur - ' . $deltap,
			),
			array(
				'gauche >= ' . $gauche,
				'droit <= ' . $droit,
		));
		$sql->update($table,
			array(
				'gauche'     => 'gauche - ' . $delta,
				'droit'      => 'droit - ' . $delta,
			),
			array(
				'gauche >= ' . $gauche,
		));
		if ($sql->run()) {
			sql_intervallaire::_regenerer_rang($table, 0);
			return true;
		}
		return false;	
	}





	/**
	 * Déplace un arbre (noeud et enfants) à gauche de la racine. 
	 *
	 * @param string $table 	Table SQL
	 * @param int $id_source 	Identifiant de l'element a déplacer
	 * @param int $unused 		Identifiant cible (inutilisé)
	 * @return bool.
	**/
	static private function _deplacer_arbre_a_gauche_de_racine($table, $id_source, $unused = null) {
		if (!$source = sql_intervallaire::get_noeud_infos($table, $id_source)) {
			return false;
		}

		$id_table = id_table_objet($table);
		$gauche   = $source['gauche'];
		$droit    = $source['droit'];
		$delta    = $droit - $gauche + 1;		
		$deltap   = $source['profondeur'] - 1;

		
		// decaler la source sur le bord racine gauche
		// augmenter du delta tout ce qui est a gauche du bord gauche
		$sql = new sql_transaction();
		$sql->update($table,
			array(
				'id_parent'     => 0,
			),
			array(
				'gauche = ' . $gauche,
				'droit = ' . $droit,
		));
		$sql->update($table,
			array(
				'gauche'     => 'gauche - ' . (2*$delta + 2),
				'droit'      => 'droit - ' . (2*$delta + 2),
				'profondeur' => 'profondeur - ' . $deltap,
			),
			array(
				'gauche >= ' . $gauche,
				'droit <= ' . $droit,
		));
		$sql->update($table,
			array(
				'gauche'     => 'gauche + ' . $delta,
				'droit'      => 'droit + ' . $delta,
			),
			array(
				'droit <= ' . $droit,
		));
		if ($sql->run()) {
			sql_intervallaire::_regenerer_rang($table, 0);
			return true;
		}
		return false;	
	}




	/**
	 * Déplace un arbre (noeud et enfants) à droite d'un parent (en bas a droite, donc). 
	 *
	 * @param string $table 	Table SQL
	 * @param int $id_source 	Identifiant de l'element a déplacer
	 * @param int $unused 		Identifiant cible (inutilisé)
	 * @return bool.
	**/
	static private function _deplacer_arbre_a_droite_de_parent($table, $id_source, $id_parent) {

		if (!$source = sql_intervallaire::get_noeud_infos($table, $id_source)) {
			return false;
		}

		// pas de parent ? pas de chocolat
		if (!$parent = sql_intervallaire::get_noeud_infos($table, $id_parent)) {
			return false;
		}
		
		$id_table   = id_table_objet($table);

		// quel type de déplacement ?
		if ($source['droit'] < $parent['gauche']) {
			// on deplace la source vers la droite dans l'arbre
			// ses deux bornes vont augmenter
			$type = 'droit'; 
		} elseif ($source['gauche'] > $parent['droit']) {
			// on deplace la source vers la gauche dans l'arbre
			// ses deux bornes vont diminuer
			$type = 'gauche';
		// entre les deux c'est plus complexe...
		} elseif (($source['gauche'] > $parent['gauche']) and ($source['droit'] < $parent['droit'])) {
			// on deplace une descendance vers un niveau plus haut
			$type = 'haut';
		} else {
			// dernière possibilité, on déplace un parent dans une de ses descendances...
			// pas simple :p
			$type = 'bas';
		}

		$delta  = $source['droit'] - $source['gauche'] + 1;
		$delta_parent  = abs($source['gauche'] - $parent['gauche']);
		
		$deltap = abs($source['profondeur'] - $parent['profondeur']) + 1;
		$opp = ($source['profondeur'] >= $parent['profondeur']) ? '+' : '-';
	
		
		// decaler la source sur le bord racine gauche
		// augmenter du delta tout ce qui est a gauche du bord gauche
		$sql = new sql_transaction();
		
		// 1) Augmenter l'intervalle du receveur
		$sql->update($table,
			array(
				'droit' => 'droit + ' . $delta,
			),
			array(
				'droit >= ' . $parent['droit']
		));
		$sql->update($table,
			array(
				'gauche' => 'gauche + ' . $delta,
			),
			array(
				'gauche >= ' . $parent['droit']
		));

		// 2) On deplace l'arbre source dans l'intervalle cree
		$op = ($type == 'gauche') ? '-' : '+';
		$sql->update($table,
			array(
				'id_parent' => $parent['id']
			),
			array(
				'gauche = ' . ($source['gauche'] + $delta),
				'droit = ' . ($source['droit'] + $delta)
		));
		$sql->update($table,
			array(
				'gauche' => "gauche $op " . ($delta_parent + 1),
				'droit' => "droit $op " . ($delta_parent + 1),
				'profondeur' => "profondeur $opp $deltap"
			),
			array(
				'gauche >= ' . ($source['gauche'] + $delta),
				'droit <= ' . ($source['droit'] + $delta)
		));
		
		// [todo] on a fait que $type = droit là...
		// 3) on redecale ce qui suit (ou precede) le parent...
		$sql->update($table,
			array(
				'gauche' => "gauche $op " . ($delta_parent),
				'droit' => "droit $op " . ($delta_parent)
			),
			array(
				'droit > ' . ($parent['droit'] + $delta) // a ameliorer
		));
#echo "\n<pre>"; print_r($sql->queries); echo "</pre>";
		if ($sql->run()) {
			// l'ancien emplacement de la source...
			sql_intervallaire::_regenerer_rang($table, $source['id_parent']);
			// au nouvel emplacement de la source...
			sql_intervallaire::_regenerer_rang($table, $parent['id']);
			return true;
		}
		return false;	
			
	}
	
}

?>
