<?php

/**
 * Classe pour faciliter la creation de transactions SQL
 * avec l'abstraction sql de SPIP 
 *
 * $t = new sql_transaction($serveur);
 * $t->update(param, param...) // = mêmes parametres que sql_update(...) 
 * $t->updateq(param, param...) // etc...
 * $t->run(); // execution
**/

class sql_transaction {
	var $queries = array();
	var $serveur = '';
	
	function __construct($serveur='') {
		$this->serveur = $serveur;
	}

	// inserer directement ce qui est transmis
	function query($query) {
		$this->queries[] = $query;
	}

	// lancer la transaction avec toutes les requetes.
	function run() {
		# [todo] bon... c'est pas ça en mysql... a trouver...
		#sql_query('BEGIN TRANSACTION', $this->serveur);
		sql_query('BEGIN', $this->serveur);
		foreach ($this->queries as $q) {
		spip_log($q);
			sql_query($q, $this->serveur);
		}
		return sql_query('COMMIT', $this->serveur);
	}

	// fonctions d'abstraction retournant la requete
	function update($table, $exp, $where='', $desc=array()) {
		$this->queries[] = sql_update($table, $exp, $where, $desc, $this->serveur, false);
	}
	
	function updateq($table, $exp, $where='', $desc=array()) {
		$this->queries[] = sql_updateq($table, $exp, $where, $desc, $this->serveur, false);
	}

	function delete($table, $where='') {
		$this->queries[] = sql_delete($table, $where, $this->serveur, false);
	}

	function insertq($table, $couples=array(), $desc=array()) {
		$this->queries[] = sql_insertq($table, $couples, $desc, $this->serveur, false);
	}
}



?>
